class RPNCalculator
  attr_accessor :calculator

  def initialize
    @stack = []

  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    operation(:+)
  end

  def minus
    operation(:-)
  end

  def times
    operation(:*)
  end

  def divide
    operation(:/)
  end


  def tokens(string)
    tokens = []
    string.split.each do |el|
      if %w( * ** / - +).include?(el)
        tokens << el.to_sym
      else
        tokens << el.to_i
      end
    end
  tokens
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |x|
      case x
      when Integer
        push(x)
      else
        operation(x)
      end
    end
    value
  end

  def operation(array)
    raise "calculator is empty" if @stack.length < 2
    pop1 = @stack.pop
    pop2 = @stack.pop

    case array
    when :+
      @stack << pop2 + pop1
    when :-
      @stack << pop2 - pop1
    when :*
      @stack << pop2 * pop1
    when :/
      @stack << pop2.to_f / pop1.to_f
    else
      @stack << pop2
      @stack << pop1
    end
  end
end
